import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';

import {Form} from './src/components/Form';
import {TasksBox} from './src/components/TasksBox';

import {GlobalStores} from './src/stores/GlobalStores';
import {StoreProvider} from './src/stores/useStore';
import {colors} from './src/extra/colors';

const stores = {
  globalStore: new GlobalStores(),
};

const App = () => (
  <StoreProvider store={stores}>
    <View style={styles.appWrapper}>
      <Text style={styles.appHeader}>ToDo List</Text>

      <Form />

      <TasksBox />
    </View>
  </StoreProvider>
);

const styles = StyleSheet.create({
  appWrapper: {
    paddingTop: getStatusBarHeight(),
    paddingHorizontal: 16,
  },
  appHeader: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 16,
    textAlign: 'center',
    color: colors.VIOLET,
  },
});

export default App;
