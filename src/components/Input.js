import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

import {colors} from '../extra/colors';

export const Input = ({
  value,
  onChangeText,
  placeholder,
  autoFocus,
  placeholderTextColor,
}) => (
  <TextInput
    value={value}
    onChangeText={onChangeText}
    style={styles.input}
    placeholder={placeholder}
    autoFocus={autoFocus}
    placeholderTextColor={placeholderTextColor}
  />
);

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: colors.LIGHT_VIOLET,
    color: colors.VIOLET,
    marginVertical: 8,
    padding: 6,
  },
});
