import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

import {colors} from '../extra/colors';

const CHECKBOX_EDGE_LENGTH = 16;

export const CheckBox = ({active, onChange}) => (
  <TouchableOpacity
    onPress={onChange}
    style={[styles.common, active ? styles.active : styles.inactive]}
  >
    {active && (
      <>
        <View style={styles.arrowLeftLine} />

        <View style={styles.arrowRightLine} />
      </>
    )}
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  common: {
    width: CHECKBOX_EDGE_LENGTH,
    height: CHECKBOX_EDGE_LENGTH,
  },
  arrowLeftLine: {
    borderBottomWidth: 1,
    borderBottomColor: colors.WHITE,
    height: 1,
    width: 7,
    position: 'absolute',
    bottom: 5,
    left: 1,
    transform: [{rotate: '60deg'}],
  },
  arrowRightLine: {
    borderBottomWidth: 1,
    borderBottomColor: colors.WHITE,
    height: 1,
    width: 10,
    position: 'absolute',
    bottom: 7,
    right: 2,
    transform: [{rotate: '120deg'}],
  },
  active: {
    backgroundColor: colors.VIOLET,
  },
  inactive: {
    borderWidth: 1,
    borderColor: colors.LIGHT_VIOLET,
  },
});
