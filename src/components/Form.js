import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {observer} from 'mobx-react';

import {Input} from './Input';
import {Button} from './Button';

import {useStore} from '../stores/useStore';
import {colors} from '../extra/colors';

const Component = () => {
  const [newTaskValue, setNewTaskValue] = useState('');
  const {globalStore} = useStore();

  const onSubmit = async () => {
    await globalStore.pushToTasks({
      title: newTaskValue,
      isComplete: false,
      id: globalStore.tasks.length + 1,
    });
    setNewTaskValue('');
  };

  return (
    <View>
      <Input
        value={newTaskValue}
        onChangeText={setNewTaskValue}
        placeholder="New task title"
        placeholderTextColor={colors.LIGHT_VIOLET}
      />

      <View style={styles.createButtonWrapper}>
        <Button onPress={onSubmit} title="Create" style={styles.button} />
      </View>
    </View>
  );
};

export const Form = observer(Component);

const styles = StyleSheet.create({
  createButtonWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: colors.VIOLET,
    backgroundColor: colors.LIGHT_VIOLET,
    width: 70,
  },
});
