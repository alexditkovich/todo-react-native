import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

export const Button = ({onPress, title, style}) => (
  <TouchableOpacity onPress={onPress} style={style}>
    <Text>{title}</Text>
  </TouchableOpacity>
);
