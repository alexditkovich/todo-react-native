import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {observer} from 'mobx-react';

import {CheckBox} from './CheckBox';
import {Button} from './Button';
import {Input} from './Input';

import {useStore} from '../stores/useStore';
import {colors} from '../extra/colors';

const Component = () => {
  const [editableTaskIndex, setEditableTaskIndex] = useState(null);
  const [editableTaskValue, setEditableTaskValue] = useState('');
  const {globalStore} = useStore();

  const handleEditTask = async index => {
    await globalStore.editTask(index, editableTaskValue);
    setEditableTaskValue('');
    setEditableTaskIndex(null);
  };

  const handlePressEditButton = (index, taskTitle) => {
    setEditableTaskIndex(index);
    setEditableTaskValue(taskTitle);
  };

  const handleDelete = async index => {
    await globalStore.deleteTask(index);
    setEditableTaskIndex(null);
    setEditableTaskValue('');
  };

  return (
    <View>
      {globalStore.tasks
        .map((task, index) => (
          <View key={task.id} style={styles.taskWrapper}>
            <View style={styles.checkboxAndTitleWrapper}>
              <CheckBox
                active={task.isComplete}
                onChange={() => globalStore.invertIsComplete(index)}
              />

              {editableTaskIndex === index ? (
                <View>
                  <Input
                    value={editableTaskValue}
                    onChangeText={setEditableTaskValue}
                    placeholder="Edit task title"
                    autoFocus
                  />
                </View>
              ) : (
                <Text
                  style={[
                    styles.taskTitleText,
                    {
                      textDecorationLine: task.isComplete
                        ? 'line-through'
                        : 'none',
                      textDecorationStyle: 'solid',
                      color: task.isComplete
                        ? colors.LIGHT_VIOLET
                        : colors.VIOLET,
                    },
                  ]}>
                  {task.title}
                </Text>
              )}
            </View>

            <View style={styles.buttonsWrapper}>
              {editableTaskIndex !== index ? (
                <Button
                  onPress={() => handlePressEditButton(index, task.title)}
                  backgroundColor={colors.VIOLET}
                  title=" Edit / "
                />
              ) : (
                <Button
                  onPress={() => handleEditTask(index)}
                  title=" Save / "
                />
              )}

              <Button
                onPress={() => handleDelete(index)}
                title="Del "
                backgroundColor="red"
              />
            </View>
          </View>
        ))
        .reverse()}
    </View>
  );
};

export const TasksBox = observer(Component);

const styles = StyleSheet.create({
  taskTitleText: {
    maxWidth: 230,
    marginLeft: 20,
  },
  taskWrapper: {
    flexDirection: 'row',
    marginTop: 16,
    justifyContent: 'space-between',
  },
  buttonsWrapper: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRightColor: colors.MEDIUM_VIOLET_RED,
    borderBottomColor: colors.MEDIUM_VIOLET_RED,
    borderLeftColor: colors.VIOLET,
    borderTopColor: colors.VIOLET,
    borderRadius: 3,
    alignSelf: 'flex-start',
  },
  checkboxAndTitleWrapper: {
    flexDirection: 'row',
  },
});
