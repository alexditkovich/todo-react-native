export const colors = {
  VIOLET: '#7703fc',
  LIGHT_VIOLET: '#ceabff',
  MEDIUM_VIOLET_RED: '#a61277',
  WHITE: '#fff',
};
