import {makeAutoObservable} from 'mobx';

import {deviceStorage, TASKS} from '../extra/deviceStorage';

export class GlobalStores {
  constructor() {
    makeAutoObservable(this);

    (async () => {
      const savedTask = await deviceStorage.get(TASKS);

      this.tasks = savedTask ? savedTask : [];
    })();
  }

  async pushToTasks(task) {
    this.tasks.push(task);
    await deviceStorage.set(TASKS, this.tasks);
  }

  async invertIsComplete(index) {
    this.tasks[index].isComplete = !this.tasks[index].isComplete;
    await deviceStorage.set(TASKS, this.tasks);
  }

  async deleteTask(index) {
    this.tasks.splice(index, 1);
    await deviceStorage.set(TASKS, this.tasks);
  }

  async editTask(index, title) {
    this.tasks[index].title = title;
    await deviceStorage.set(TASKS, this.tasks);
  }

  tasks = [];
}
